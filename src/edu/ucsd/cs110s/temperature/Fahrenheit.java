/**
 * 
 */
package edu.ucsd.cs110s.temperature;

public class Fahrenheit extends Temperature {
	public Fahrenheit(float t) {
		super(t);
	}

	public String toString() {
		return Float.toString(getValue());
	}

	@Override
	public Temperature toCelsius() {
		return new Celsius((float) ((getValue() - 32) * 5. / 9));
	}

	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}
