/**
 * 
 */
package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	public Celsius(float t) {
		super(t);
	}

	public String toString() {
		return Float.toString(getValue());
	}

	@Override
	public Temperature toCelsius() {

		return this;
	}

	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((float) (getValue() * (9.0 / 5) + 32));
	}
}